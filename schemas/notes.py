from __future__ import annotations
from marshmallow import (
    Schema,
    fields,
    validate,
    post_load,
)
from models.notes import Notes


class NotesSchema(Schema):
    title = fields.Str(
        attribute="title",
        data_key="title",
        required=True,
        validate=validate.Length(error="Title invalid", min=6, max=200)
    )
    description = fields.Str(
        attribute="description",
        data_key="description",
        required=True,
        validate=[validate.Length(error="Description invalid", min=6, max=36)]
    )

    @post_load
    def make_note(self, data, **kwargs) -> Notes:
        return Notes(**data)

    class Meta:
        strict = True
