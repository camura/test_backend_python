from marshmallow import (
    Schema,
    fields,
    post_load,
)


class AuthenticationSchema(Schema):
    username = fields.Str(
        attribute="username",
        data_key="username",
        required=True
    )
    password = fields.Str(
        attribute="password",
        data_key="password",
        required=True
    )

    @post_load
    def make_note(self, data, **kwargs):
        return {'username': data['username'], 'password': data['password']}

    class Meta:
        strict = True
