from __future__ import annotations
from peewee import CharField, PrimaryKeyField, Model
from connection_db.connection_db import db
import jwt


class Users(Model):
    id = PrimaryKeyField(unique=True)
    username = CharField(max_length=200)
    password = CharField()

    @staticmethod
    def authenticate(username, password) -> Users:
        try:
            user = Users.get(
                Users.username == username, Users.password == password
            )
        except Exception:
            return None
        return user

    def get_jwt(self):
        key = 'xhtesare'
        data = {
            'sub': self.username,
            'username': self.username
        }
        encoded = jwt.encode(
            data, key
        )
        return str(encoded)

    class Meta:
        database = db
