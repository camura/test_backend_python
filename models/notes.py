from peewee import CharField, TextField, PrimaryKeyField, Model
from connection_db.connection_db import db


class Notes(Model):
    id = PrimaryKeyField(unique=True)
    title = CharField(max_length=200)
    description = TextField()

    @staticmethod
    def list():
        query = Notes.select().dicts()
        result = []
        for row in query:
            result.append(row)
        return result

    @staticmethod
    def create(title, description):
        Notes(
            title=title,
            description=description
        ).save(force_insert=True)

    class Meta:
        database = db
