from connection_db.connection_db import db
from models.notes import Notes
from models.users import Users

# connect
db.connect()

# create tables
db.create_tables([Notes, Users])

# create Notes
note_one = Notes(
    title='Nota numero Uno',
    description='Descripcion para nota numero uno').save(force_insert=True)
note_two = Notes(
    title='Nota numero Dos',
    description='Descripcion para nota numero dos').save(force_insert=True)

# create users
user = Users(
    username='camura',
    password='camura').save(force_insert=True)

# disconnect
db.close()
