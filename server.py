# Run with "python server.py"
from marshmallow import ValidationError
from bottle import get, run, response, post, request, HTTPResponse
from models.notes import Notes
from models.users import Users
from schemas.notes import NotesSchema
from schemas.authentication import AuthenticationSchema
import json


@get('/notes')
def notes_list():
    response.content_type = 'application/json'
    schema = NotesSchema(many=True)
    data = schema.dump(Notes.list())
    return json.dumps(data)


@post('/notes')
def notes_create():
    response.content_type = 'application/json'
    data = request.json
    schema = NotesSchema()
    try:
        note, errors = schema.load(data)
    except ValidationError as err:
        errors = err.messages
        headers = {'Content-type': 'application/json'}
        return HTTPResponse(
            status=400, body=json.dumps(errors), headers=headers
        )
    note.save(force_insert=True)
    return json.dumps({'result': 'ok'})


@post('/authentication')
def user_authenticate():
    response.content_type = 'application/json'
    headers = {'Content-type': 'application/json'}
    data = request.json
    schema = AuthenticationSchema()
    try:
        result, errors = schema.load(data)
    except ValidationError as err:
        errors = err.messages
        return HTTPResponse(
            status=401, body=json.dumps(errors), headers=headers
        )
    user = Users.authenticate(
        username=result['username'],
        password=result['password']
    )
    if not user:
        return HTTPResponse(
            status=401, headers=headers
        )
    return json.dumps({'jwt': user.get_jwt()})


run(host='localhost', port=8000)
